/*
    i2c_streamer.cpp - I2C Device Output Streamer

    Copyright (C) 2015 AndPlus LLC

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    MA 02110-1301 USA.
*/

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

#include <linux/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <i2c-dev.h>

extern "C" {
    #include <smbus.h>
}

#define BUS_NUMBER 2
#define DEVICE_ADDR 8

 #include <time.h>


template<typename T>
T parse(std::string s)
{
    T result;
    std::stringstream ss;
    ss << s;
    ss >> result;
    return result;
}

void sleepMS(int msCount)
{
    timespec t;
    t.tv_sec = 0;
    t.tv_nsec = 1000000 * msCount;
    nanosleep(&t, NULL);
}

int main(int argc, char** argv)
{
    if (argc < 4)
    {
        std::cerr << "USAGE: " << argv[0] << " I2C_BUS I2C_DEVICE_ADDR COMMAND_0 COMMAND_1 COMMAND_2" << std::endl;
        return 1;
    }
    
    int i2cBusNum = parse<int>(argv[1]);
    
    int deviceAddr = parse<int>(argv[2]);
    if (deviceAddr < 0 || deviceAddr > 255)
    {
        std::cerr << "Invalid device address: " << deviceAddr << std::endl;
    }
    
    std::vector<__u8> commands;
    for (int i = 3; i < argc; i++)
    {
        int command = parse<int>(argv[i]);
        if (command <= 0 || command > 255)
        {
            std::cerr << "Invalid command: " << command << std::endl;
            return 1;
        }
        commands.push_back(static_cast<__u8>(command));
    }
    
    std::stringstream busName;
    busName << "/dev/i2c-" << i2cBusNum;
    
    int fd = open(busName.str().c_str(), O_RDWR);
    if (fd == 0)
    {
        std::cerr << "Could not open " << busName << std::endl;
        return 1;
    }
    
        
    if (ioctl(fd, I2C_SLAVE, static_cast<__u8>(deviceAddr)) < 0)
    {
        std::cerr << "Could not attach to device " << deviceAddr << std::endl;
        return 1;
    }
    
    while (true)
    {
        for (int i = 0; i < commands.size(); i++)
        {
            int command = commands[i];
            i2c_smbus_read_word_data(fd, command);
            
            sleepMS(10);
            
            result = i2c_smbus_read_word_data(fd, command);
            if (result < 0)
            {
                std::cout << std::hex << command << " " << result << std::endl;
                sleepMS(100);
                continue;
            }
            int result = (((result & 0xFF) << 8) | ((result & 0xFF00) >> 8)); //Flip endianness
            std::cout << std::hex << command << " " << result << std::endl;
        }
    }
    
    
    //TODO: Exit condition?
    close(fd);
    return 0;
}
