LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS) 
# give module name
LOCAL_MODULE    := i2c_streamer  
# list your C files to compile
LOCAL_SRC_FILES := i2c_streamer.cpp smbus.c
# this option will build executables instead of building library for android application.
include $(BUILD_EXECUTABLE)
